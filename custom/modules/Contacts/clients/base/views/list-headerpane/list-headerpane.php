<?php

$viewdefs['Contacts']['base']['view']['list-headerpane'] = array(
    'template' => 'headerpane',
    'title' => 'LBL_MODULE_NAME',
    'buttons' => array(
        array(
            'name' => 'contact_button',
            'type' => 'button',
            'label' => 'Contact',
            'css_class' => 'btn-success',
            'acl_action' => 'list',
            'events' => array(
                'click' => 'list:contact_instalog_button:fire',
            ),
        ),
        array(
            'name' => 'create_button',
            'type' => 'button',
            'label' => 'LBL_CREATE_BUTTON_LABEL',
            'css_class' => 'btn-primary',
            'acl_action' => 'create',
            'route' => array(
                'action' => 'create',
            ),
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);
