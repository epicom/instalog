/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
({
    plugins: ['Dashlet'],
    selectedRows: {},
    fetchedModel: {},
    currentKey: 0,
    path: '',
    moduleSingular: '',
    modulePlural: '',

    initialize: function(options) {
      this._super('initialize', [options]);

      this.context.on('list:contact_instalog_button:fire', this.applyContactFxn, this);
      this.context.on('list:lead_instalog_button:fire', this.applyLeadFxn, this);
    },

    events: _.extend({
      "click a[name=go_left]": "goLeft",
      "click a[name=go_right]": "goRight",
      "click a[name=log_activity]": "logActivity",
      "click a[name=quick_log_activity]": "quickLogCall",
      "change #lead-status": "updateLeadStatus"
    }),

    quickLogCall: function() {
      var callBean = app.data.createBean("Calls");

      var defaultDateTime = app.date().seconds(0);

      var description = $("#comment").val();

      var name = "Call on " + defaultDateTime.formatUser(true) + " " + defaultDateTime.format(app.date.getUserTimeFormat())

      callBean.set({
          name: name + " " + description.substring(0, 50),
          date_end: defaultDateTime.formatServer(),
          date_start: defaultDateTime.add('m', -5).formatServer(),
          duration_hours: 0,
          duration_minutes: 5,
          parent_id: this.selectedRows.models[this.currentKey].get('id'),
          parent_type: this.selectedRows.models[this.currentKey].get('_module'),
          status: "Held",
          description: description
      });

      callBean.save();
      app.alert.show('success_after_quick_call', {
          level:'success',
          title: 'Success:',
          autoClose: true,
          messages: 'A call was succesfully logged for the selected '+this.moduleSingular+'.'
      });

      this.goRight();
    },

    logActivity: function() {
      var modelPrefil = app.data.createBean($("#activity_type").val());
      modelPrefil.set({
        description: $("#comment").val(),
        parent_type: this.modulePlural,
        parent_id: this.selectedRows.models[this.currentKey].get('id'),
        parent_name: this.selectedRows.models[this.currentKey].get('full_name'),
      });
      if ($("#activity_type").val() === 'Tasks' || $("#activity_type").val() === 'Notes') {
        modelPrefil.set({
          contact_id: this.selectedRows.models[this.currentKey].get('id'),
          contact_name: this.selectedRows.models[this.currentKey].get('full_name'),
        })
      }
      app.drawer.open({
        layout: 'create',
        context: {
          create: true,
          module: $("#activity_type").val(),
          model: modelPrefil
        }
      });
    },

    goLeft: function() {
      if (this.currentKey > 0) {
          this.currentKey--;
      } else {
          this.currentKey = this.selectedRows.length - 1;
      }

      this.showInfo();
    },

    goRight: function() {
      if (this.currentKey < this.selectedRows.length - 1) {
          this.currentKey++;
      } else {
          this.currentKey = 0;
      }

      this.showInfo();
    },

    updateLeadStatus: function() {
      // show a tiny loading/saving indicator
      $('#lead-status-saving').show();

      var params = {
        id: this.selectedRows.models[this.currentKey].get('id'),
        status: $('#lead-status').val()
      };

      app.api.call('create', app.api.buildURL('instalog/update-lead-status'), params, {
        success: function(data) {
          if (data === 1) {
            $('#lead-status-saving').hide();
          }
        }
      });
    },

    applyLeadFxn: function() {
      this.path = 'get-lead';
      this.moduleSingular = 'Lead';
      this.modulePlural = 'Leads';

      this.applyFxn();
    },

    applyContactFxn: function() {
      this.path = 'get-contact';
      this.moduleSingular = 'Contact';
      this.modulePlural = 'Contacts';

      this.applyFxn();
    },

    applyFxn: function() {
      // show the loading gif
      $('#tracker-loading').show();
      // update the text
      $('#no-records-text').text('Loading...');

      this.selectedRows = this.context.attributes.mass_collection;

      if (!this.selectedRows.length) {
          app.alert.show('error_before_contact', {
              level:'error',
              title: 'Error:',
              messages: 'Select the rows you desire more information on!'
          });
          return false;
      }

      this.showInfo();
    },

    showInfo: function() {
      var view = this;
      var id = this.selectedRows.models[this.currentKey].get('id');

      app.api.call('read', app.api.buildURL('instalog/' + this.path + '/' + id), null, {
          success: function(response) {
            historyParams = {
              max_num: 3,
              order_by: 'date_start:DESC'
            };
            app.api.call('read', app.api.buildURL(view.modulePlural+'/'+id+'/link/history'), historyParams, {
              success: function(data) {
                // hide the loading gif
                $('#tracker-loading').hide();

                response.historicalSummary = data.records;

                view.fetchedModel = response;
                view.render();
              }
            });
          }
      });
    }
})
