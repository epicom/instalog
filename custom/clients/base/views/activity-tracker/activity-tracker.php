<?php

$viewdefs['base']['view']['activity-tracker'] = array(
    'dashlets' => array(
        array(
            'label' => 'Instant Log',
            'description' => 'Instant Log dashlet for Contacts and Leads',
            'config' => array(
            ),
            'preview' => array(
            ),
            'filter' => array(
                'module' => array(
                    'Leads',
                    'Contacts',
                ),
                'view' => array(
                    'records',
                ),
            ),
        ),
    ),
);
