<?php

class InstalogApi extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'GetContact' => array(
                'reqType' => 'GET',
                'path' => array('instalog', 'get-contact', '?'),
                'pathVars' => array('dashlet', 'method', 'id'),
                'method' => 'getContactInformation',
                'shortHelp' => 'Gets information about a Contact',
                'longHelp' => '',
            ),
            'GetLead' => array(
                'reqType' => 'GET',
                'path' => array('instalog', 'get-lead', '?'),
                'pathVars' => array('dashlet', 'method', 'id'),
                'method' => 'getLeadInformation',
                'shortHelp' => 'Gets information about a Lead',
                'longHelp' => '',
            ),
            'UpdateLeadStatus' => array(
                'reqType' => 'POST',
                'path' => array('instalog', 'update-lead-status'),
                'pathVars' => array('dashlet', 'method'),
                'method' => 'updateLeadStatus',
                'shortHelp' => 'Updates Lead Status on Select Change',
                'longHelp' => '',
            ),
        );
    }

    public function getContactInformation($api, $args)
    {
        global $app_list_strings;

        if ($args['id']) {
            $contact = BeanFactory::getBean('Contacts', $args['id']);

            $cleanContact = new stdClass();

            $cleanContact->id = $contact->id;
            $cleanContact->name = $contact->name;
            $cleanContact->my_favorite = $contact->my_favorite;
            $cleanContact->account_name = $contact->account_name;
            $cleanContact->account_id = $contact->account_id;
            $cleanContact->title = $contact->title;
            $cleanContact->phone_work = $contact->phone_work;
            $cleanContact->phone_mobile = $contact->phone_mobile;
            $cleanContact->email1 = $contact->email1;
            $cleanContact->description = $contact->description;
            $cleanContact->current_crm = $app_list_strings['current_crm_list'][$contact->current_crm_c];

            $cleanContact->_module = 'Contacts';

            return $cleanContact;
        }

        return '';
    }

    public function getLeadInformation($api, $args)
    {
        global $app_list_strings;

        if ($args['id']) {
            $lead = BeanFactory::getBean('Leads', $args['id']);

            $cleanLead = new stdClass();

            $cleanLead->id = $lead->id;
            $cleanLead->name = $lead->name;
            $cleanLead->status = $lead->status;
            $cleanLead->my_favorite = $lead->my_favorite;
            $cleanLead->account_name = $lead->account_name;
            $cleanLead->title = $lead->title;
            $cleanLead->phone_work = $lead->phone_work;
            $cleanLead->phone_mobile = $lead->phone_mobile;
            $cleanLead->email1 = $lead->email1;
            $cleanLead->description = $lead->description;
            $cleanLead->current_crm = $app_list_strings['current_crm_list'][$lead->current_crm_c];

            $cleanLead->status_list = $app_list_strings['lead_status_dom'];
            $cleanLead->_module = 'Leads';

            return $cleanLead;
        }

        return '';
    }

    public function updateLeadStatus($api, $args)
    {
        if ($args['id']) {
            $lead = BeanFactory::getBean('Leads', $args['id']);

            $lead->status = $args['status'];

            $lead->save();

            return 1;
        }

        return 0;
    }
}
