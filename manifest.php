<?php

$manifest = array(
  'built_in_version' => '7.6.0.0',
  'acceptable_sugar_versions' => array(
    0 => '',
  ),
  'acceptable_sugar_flavors' => array(
    0 => 'ENT',
    1 => 'ULT',
    2 => 'PRO',
    3 => 'CORP',
  ),
  'readme' => '',
  'key' => 'raw',
  'author' => 'Epicom::DannyMulvihill()',
  'description' => 'Allow quick call logging for Leads and Contacts',
  'icon' => '',
  'is_uninstallable' => true,
  'name' => 'Instalog',
  'published_date' => '2016-08-09',
  'type' => 'module',
  'version' => '0.2.2',
  'remove_tables' => 'prompt',
);

$installdefs = array(
  'copy' => array(
    0 => array(
      'from' => '<basepath>/custom',
      'to' => 'custom',
    ),
  ),
);
